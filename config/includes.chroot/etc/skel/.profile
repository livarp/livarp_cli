#!/bin/bash
source ~/.bashrc
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
clear
echo ""
echo ""
echo -e "${cyan}welcome to ${red}Debian${cyan} livarp_cli system"
echo
echo -e "${red}           ------------------------------"
echo -e "${red}           $NC Debian GNU/Linux livarp 04$NC"
echo -e "${red}           ------------------------------"
echo -e "${red} ${yellow} kernel information"
echo -e "${red} ${yellow} $NC `uname -a`"
echo -e "${red} ${green} machine stats"
echo -e "${red} ${green} $NC`uptime`"
echo -e "${red} ----------------------------------------$NC"
echo ""
echo ""
echo ""
sleep 4s
## console layout config # safe to remove after installation
## ---------------------------------------------------------
if [ -d /home/human ];then
    echo -e "${cyan} console keyboard selection:"
    echo -e " ---------------------------$NC"
    echo ""
    echo " f : fr"
    echo " b : be"
    echo " e : es"
    echo " r : ru"
    echo " k : uk"
    echo " u : us"
    echo " d : de"
    echo ""
    echo -e "${cyan} type a letter to load your keyboard layout >>$NC"
    read kb
    case $kb in
        f) sudo loadkeys fr-pc ;;
        b) sudo loadkeys be-latin1 ;;
        e) sudo loadkeys es ;;
        r) sudo loadkeys ru ;;
        k) sudo loadkeys uk ;;
        u) sudo loadkeys us ;;
        d) sudo loadkeys de ;;
        *) sudo loadkeys us ;;
    esac
fi
## launch dvtm console manager
## ---------------------------
dvtm h
